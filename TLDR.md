

| File                     | Notables | Location                   | Quest                             | Loot                                                       | Notes                                        |
| ------------------------ | -------- | -------------------------- | --------------------------------- | ---------------------------------------------------------- | -------------------------------------------- |
| [[session_7_2024-04-27]] |          | • Barley Manor             | • Purge the blight                | • Mahito of healing<br>• Goop sample<br>• Summoning circle | • Therian joined                             |
| [[session_8_2024-05-04]] |          | • Northern Road<br>• Teais | • Question of Bat<br>• SHOPPING!~ | • Marshusrex Box<br>• Shoppe purchases                     | • Found a man who knows about the green goop |


File: link to session notes
Notables: Notable people we ran into, DMPC, NPC, AltPC, etc
Location: Where
Quest: what we did
Loot: what we got
Notes: other short notes about the session
